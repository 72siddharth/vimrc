" Native Vim Settings
set encoding=utf-8
set clipboard+=unnamedplus
set foldmethod=indent
set foldlevel=99
set scrolloff=8
set splitright
set splitbelow
set hidden
filetype plugin indent on

" Search
nnoremap <esc><esc> :silent! nohls<cr>

" KeyMaps - Buffer Navigation
nnoremap gn :bn<CR>
nnoremap gp :bp<CR>

" Keymaps - Split Navigation
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

" Keymaps - Terminal Mode
tnoremap <C-N> <C-\><C-n>

" Keymaps - Code Folding
noremap <space> za


" Netrw
noremap <C-E> :Lexplore<CR>

let g:netrw_banner=0
let g:netrw_liststyle=3
let g:netrw_browse_split=4
let g:netrw_list_hide= '.*\.png$,.*\.pdf,.*\.mp4,.*\.tga,.*\.mp3,.*\.jpg,.*\.svg,/*\.stl,.*\.mtl,.*\.ply' 
let g:netrw_winsize=20

function! NetrwMapping()
	noremap <buffer> <C-l> <C-w>l
	noremap <C-E> :Lexplore<CR>
endfunction

augroup netrw_mapping
	autocmd!
	autocmd filetype netrw call NetrwMapping()
augroup END


" Statusline
set statusline=
" set statusline+=%#LineNr#
set statusline+=%#String#
set statusline+=\ [%n]:
set statusline+=%#Constant#
set statusline+=\ %f
set statusline+=%#Keyword#
set statusline+=\ %{fugitive#statusline()}\  
set statusline+=%{&modified?'[~]':''}
set statusline+=%{&readonly?'[=]':''}
set statusline+=%=
set statusline+=%#String#
set statusline+=\ %c:%l\/%L
set statusline+=\ %(%{&enc}%)
set statusline+=\ %{&filetype!=#''?&filetype:'none\ '}
set statusline+=\ %(%{&fileformat}%)
set statusline+=\ 
set laststatus=2


" External Plugin Calls
call plug#begin(stdpath('data') . '/plugged')
Plug 'timonv/vim-cargo'
Plug 'dylanaraps/wal.vim'
Plug 'rust-lang/rust.vim'
Plug 'AlessandroYorba/Alduin'
Plug 'Raimondi/delimitMate'
Plug 'tpope/vim-fugitive'
Plug 'ryanoasis/vim-devicons'
call plug#end()


" Colorscheme
colorscheme wal

" Transparent Background
highlight Normal guibg=NONE
highlight Normal ctermbg=NONE


" Startup Screen
fun! Start()
	if argc() == 0
		if winwidth(0) >= 55
			" New empty buffer
			enew

			" Properties for buffer
			setlocal
				\ bufhidden=wipe
				\ buftype=nofile
				\ nobuflisted
				\ nolist
				\ nonumber
				\ noswapfile
				\ norelativenumber

			" Header
			for line in split(system('cat ~/Documents/vim-header'), '\n')
				call append('$', '    ' . l:line)
			endfor
			" Gaps
			call append('$', '')

			" Todo
			let count=1
			call append('$', ' > Todo')
			for line in split(system('cat ~/Documents/todolist'), '\n')
				call append('$', '  [' . count . ']: ' . l:line)
				let count+=1
			endfor
			" Gaps
			call append('$', '')

			" Print 10 recently viewed files
			call append('$', ' > Recent')
			let count1=0
			for line in split(execute('ol'), '\n')
				let line=substitute(substitute(get(split(line, ':'), 1, ''), " ", "", ""), " ", "\\\\ ", "")
				if l:line ==? 'term'
					continue
				end
				let count1+=1
				call append('$', '  [' . count1 . ']: ' . l:line)
				if count1>=10
					break
				end
			endfor
			" Gaps
			call append('$', '')

			" Cookie for luck
			call append('$', ' > Fortune')
			for line in split(system('fortune -s -n 100'), '\n')
				call append('$', '   ' . l:line)
			endfor

			" Unmodifiable
			setlocal nomodifiable nomodified

			" Colourz
			setlocal syntax=startscreen

			" New empty bufffer when inserting
			noremap <buffer><silent> e :enew<CR>
			noremap <buffer><silent> i :enew <bar> startinsert<CR>
			noremap <buffer><silent> o :enew <bar> startinsert<CR>
		end
	end
endfun

fun! Explorer()
	if winwidth(0)>=60
		execute("Lexplore")
	endif
endfun

" Run after everything loads
autocmd VimEnter * call Start()
