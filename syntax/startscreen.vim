syntax match grouping "^ > [a-zA-Z]*"
syntax match pointer  "^  \[[0-9]*\]:"
syntax match path     "/.*/.*"

highlight default link grouping Keyword
highlight default link pointer  Character
highlight default link path     Constant
